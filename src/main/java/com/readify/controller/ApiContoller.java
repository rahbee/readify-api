package com.readify.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.readify.model.Fibonacci;
import com.readify.model.Token;
import com.readify.model.Triangle;
import com.readify.model.Word;

@RestController
@RequestMapping("/api")
public class ApiContoller {
	
	@RequestMapping(value="/Token", method=RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> getToken(){
		final HttpHeaders httpHeaders= new HttpHeaders();
	    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		return (new ResponseEntity<String>((new Token()).getTokenId(), httpHeaders, HttpStatus.OK));
	}

	@RequestMapping(value="/Fibonacci", method=RequestMethod.GET)
	public long getFibonacci(@RequestParam(value="n") long n){
		
		return (new Fibonacci()).fib(n);
	}
	
	@RequestMapping(value="/ReverseWords", method=RequestMethod.GET)
	public String getFibonacci(@RequestParam(value="sentence") String sentence){
		
		return (new Word()).reverseWord(sentence);
	}
	
	@RequestMapping(value="/TriangleType", method=RequestMethod.GET)
	public String getFibonacci(@RequestParam(value="a") int a, @RequestParam(value="b") int b, @RequestParam(value="c") int c){
		
		return (new Triangle(a,b,c)).triangleType();
	}
}
