package com.readify.model;

public class Word {
	
	public String reverseWord(String sentence){
		String[] words = sentence.split(" ");
		StringBuffer invertedSentece = new StringBuffer();
		for (String word : words){
		    StringBuffer sb = new StringBuffer(word);
		    invertedSentece.append(sb.reverse());
		    invertedSentece.append(" ");
		}
		return invertedSentece.toString();
	}
	
}
