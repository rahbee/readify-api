package com.readify.model;

public class Fibonacci {
	
	public long fib(long n){
		if (n == 1)
            return 0;
        if (n == 2)
            return 1;
 
        return fib(n - 1) + fib(n - 2);
	}

}
