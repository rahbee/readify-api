package com.readify.model;

public class Triangle {
	private int sideA;
	private int sideB;
	private int sideC;
	
	public Triangle(int a, int b, int c){
		this.sideA = a;
		this.sideB = b;
		this.sideC = c;
	}
	
	private boolean feasibile(){
		return (sideA >= sideB + sideC || sideB >= sideA + sideC || sideC >= sideA + sideB) ? false : true;
	}
	
	public String triangleType(){
		if(feasibile()){
			if(sideA == sideB && sideB == sideC){
				return "Equilateral"; 
			}else if(sideA == sideB || sideB == sideC || sideC == sideA){
				return "Isosceles";
			}else{
				return "Scalene";
			}
		}else{
			return "Error";
		}
	}

}
